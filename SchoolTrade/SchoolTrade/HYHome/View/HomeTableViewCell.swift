//
//  HomeTableViewCell.swift
//  SchoolTrade
//
//  Created by YWQ on 3/9/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
class HomeTableViewCell: UITableViewCell {
    private let ImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.green
        let image = UIImage(named: "default")
        imageView.image = image
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    private let titleLabel:UILabel = {
        let titleLabel = UILabel()
        titleLabel.text = "no title"
        return titleLabel
    }()
    
    private let priceLablel:UILabel = {
        let priceLablel = UILabel()
        priceLablel.text = "no price"
        return priceLablel
    }()
    
    private let locationLablel:UILabel = {
        let locationLablel = UILabel()
        locationLablel.text = "no location"
        return locationLablel
    }()
    
    private let createDateLablel:UILabel = {
        let createDateLablel = UILabel()
        createDateLablel.text = "no date"
        return createDateLablel
    }()
    
    var data:GoodsModel?{
        didSet{
            self.titleLabel.text = data?.title
            self.priceLablel.text = data?.price
            self.createDateLablel.text = data?.createDate
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        self.contentView.backgroundColor = UIColor.gray
        self.contentView.addSubview(ImageView)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(priceLablel)
        self.contentView.addSubview(locationLablel)
        self.contentView.addSubview(createDateLablel)
    }
    override func layoutSubviews() {
        print("layout")
        self.ImageView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
            make.width.equalTo(self.ImageView.snp.height)
        }
        self.titleLabel.snp.makeConstraints({ (make) in
            make.left.equalTo(self.ImageView.snp.right).offset(20)
            make.height.equalTo(30)
            make.top.equalTo(10)
            make.width.equalTo(100)
        })
        self.priceLablel.snp.makeConstraints { (make) in
            make.left.equalTo(self.ImageView.snp.right).offset(20)
            make.height.equalTo(20)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(5)
            make.width.equalTo(100)
        }
        self.locationLablel.snp.makeConstraints { (make) in
            make.left.equalTo(self.ImageView.snp.right).offset(20)
            make.height.equalTo(20)
            make.top.equalTo(self.priceLablel.snp.bottom).offset(5)
            make.width.equalTo(100)
        }
        
        self.createDateLablel.snp.makeConstraints { (make) in
            make.left.equalTo(self.ImageView.snp.right).offset(20)
            make.height.equalTo(20)
            make.top.equalTo(self.locationLablel.snp.bottom).offset(5)
            make.width.equalTo(100)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


