//
//  GoodsMode.swift
//  SchoolTrade
//
//  Created by YWQ on 2/25/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation

//
struct GoodsModel{
    var image:String
    var title:String
    var price:String
    var ownerName:String
//    var location:String
    var createDate:String
}
