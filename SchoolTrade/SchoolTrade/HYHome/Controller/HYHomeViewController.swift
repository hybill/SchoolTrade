//
//  HYHomeViewController.swift
//  SchoolTrade
//
//  Created by YWQ on 2/25/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
class HYHomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var data = [GoodsModel]()
    let tableView = UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.gray
        self.tableView.backgroundColor = UIColor.white
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(HomeTableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(self.tableView)
        loadData()
    }
    override func viewDidLayoutSubviews() {
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.right.equalTo(0)
            make.left.equalTo(0)
        }
    }
    
    func loadData(){
        print("")
        let data2 = [
            GoodsModel(image: "image1", title:"title1",price:"price1", ownerName: "name1", createDate: "2017-1-2"),
            GoodsModel(image: "image1",title:"title2",price:"price2", ownerName: "name2", createDate: "2017-1-2"),
            GoodsModel(image: "image1",title:"title3",price:"price3", ownerName: "name3", createDate: "2017-1-2"),
            GoodsModel(image: "image1",title:"title4",price:"price4", ownerName: "name4", createDate: "2017-1-2")]
        for i in data2{
            data.append(i)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = HomeTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        cell.data = data[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = GoodsDetailsVC()
        vc.title = data[indexPath.row].title
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
