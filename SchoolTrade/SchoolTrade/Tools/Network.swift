//
//  Network.swift
//  SchoolTrade
//
//  Created by YWQ on 2/25/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import Alamofire

protocol NetworkToolsProtocol {
    //主页商品数据
    static func LoadHomeData(completionHandler:@escaping (_ data:[GoodsModel])->())
}

extension NetworkToolsProtocol{
    static func LoadHomeData(completionHandler: @escaping ([GoodsModel]) -> ()) {
        print("load data")
    }
    
}

struct NetworkTools:NetworkToolsProtocol{
}
