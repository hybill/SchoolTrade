//
//  HYBuyerViewController.swift
//  SchoolTrade
//
//  Created by YWQ on 2/26/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
class HYOrdersViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    let data = ["11","22","33","44","55","66","77"]
    let tableView = UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blue
        self.title = "订单"
        self.tableView.delegate = self
        self.tableView.dataSource = self
//        self.tableView.rowHeight = 100
        self.view.addSubview(tableView)
        
        let publishButton:UIBarButtonItem = {
            let image = UIImage(named: "发布")
            let bt = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(publishPage))
            return bt
        }()
        self.navigationItem.rightBarButtonItem = publishButton
    }
    override func viewDidLayoutSubviews() {
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.right.equalTo(0)
            make.left.equalTo(0)
            make.bottom.equalTo(0)
        }
    }
    
    @objc func publishPage(){
        print("go publish page")
        self.navigationController?.pushViewController(HYMypublishViewController(), animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: nil)
        cell.backgroundColor = UIColor.blue
        cell.textLabel?.text = data[indexPath.row]
        return cell
    }
}

