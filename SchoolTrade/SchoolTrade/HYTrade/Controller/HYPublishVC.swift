//
//  HYPublishVC.swift
//  SchoolTrade
//
//  Created by YWQ on 3/12/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class HYPublishVC: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = HYPublishView()
    }
}
