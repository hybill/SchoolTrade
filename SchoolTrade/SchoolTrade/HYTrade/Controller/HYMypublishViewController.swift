//
//  HYSellerViewController.swift
//  SchoolTrade
//
//  Created by YWQ on 2/26/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
class HYMypublishViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    let data = ["1","2","3","4","5","6","7"]
    let id = "id"
    let tableView = UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.green
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.title = "我发布的订单"
        
        self.view.addSubview(self.tableView)
    }
    
    override func viewDidLayoutSubviews() {
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.right.equalTo(0)
            make.left.equalTo(0)
            make.bottom.equalTo(0)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == data.count{
           let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: nil)
           cell.textLabel?.text = "+"
           return cell
        }else{
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: id)
            cell.textLabel?.text = data[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        if indexPath.row == data.count{
            print("next")
            self.navigationController?.pushViewController(HYPublishVC(), animated: true)
        }
    }
}
