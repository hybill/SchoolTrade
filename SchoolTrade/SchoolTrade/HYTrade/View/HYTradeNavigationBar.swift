//
//  HYTradeNavigationBar.swift
//  SchoolTrade
//
//  Created by YWQ on 2/27/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class HYTradeNavigationBar: UIView {
    public lazy var SellerButton:UIButton = {
        let Button = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
        Button.setTitle("我发布的", for: UIControlState.normal)
        Button.layer.borderWidth = 1
        //        self.SellerButton.layer.borderColor = UIColor.white.cgColor
        Button.backgroundColor = UIColor.blue
        Button.setTitleColor(UIColor.white, for: UIControlState.normal)
        Button.setTitleColor(UIColor.green, for: UIControlState.disabled)
        return Button
    }()
    public lazy var BuyerButton:UIButton = {
        let Button = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
        Button.setTitle("我的订单", for: UIControlState.normal)
        Button.layer.borderWidth = 1
        Button.backgroundColor = UIColor.blue
        //        self.BuyerButton.layer.borderColor = UIColor.white.cgColor
        Button.setTitleShadowColor(UIColor.blue, for: UIControlState.normal)
        Button.setTitleColor(UIColor.white, for: UIControlState.normal)
        Button.setTitleColor(UIColor.green, for: UIControlState.disabled)
        return Button
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        self.addSubview(SellerButton)
        self.addSubview(BuyerButton)

    }
    override func layoutSubviews() {
        self.SellerButton.snp.makeConstraints { (make) in
            make.top.equalTo(2)
            make.bottom.equalTo(2)
            make.left.equalTo(0)
            make.width.equalTo(self.snp.width).dividedBy(2)
        }
        self.BuyerButton.snp.makeConstraints { (make) in
            make.top.equalTo(2)
            make.bottom.equalTo(2)
            make.right.equalTo(0)
            make.width.equalTo(self.snp.width).dividedBy(2)
        }
    }
   
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
