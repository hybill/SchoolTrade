//
//  HYPublishView.swift
//  SchoolTrade
//
//  Created by YWQ on 3/12/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class HYPublishView: UIView {
    
    
    let titleLabel:UILabel = {
        let title = UILabel()
        title.text = "输入标题"
        return title
    }()
    let titleTF:UITextField = {
        let textField = UITextField()
        return textField
    }()
    
    let imageView:UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(titleLabel)
        self.addSubview(titleTF)
//        self.addSubview(imageView)
    }
    override func layoutSubviews() {
        // ui 以titlelabel 对齐
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(20)
            make.height.equalTo(15)
            make.width.equalTo(40)
        }
        titleTF.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(5)
            make.left.equalTo(titleLabel.snp.left)
            make.height.equalTo(50)
            make.right.equalTo(-20)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
